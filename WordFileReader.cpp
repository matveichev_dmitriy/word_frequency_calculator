#include "WordFileReader.h"

WordFileReader::WordFileReader(const std::string& file_path):
    input_file(file_path)
{

}

WordFileReader::~WordFileReader()
{
    input_file.close();
}

std::string WordFileReader::readNext()
{
    char current_character;
    while (input_file.get(current_character) && !std::isalpha(current_character))
    {
    }

    if (!input_file.good())
        return {};

    std::string next;
    next += std::tolower(current_character);

    while (input_file.get(current_character) && std::isalpha(current_character))
    {
        next += std::tolower(current_character);
    }

    return next;
}