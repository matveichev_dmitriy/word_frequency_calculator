#include <iostream>
#include <unordered_map>
#include <set>
#include "WordFileReader.h"

using WordFrequencies = std::unordered_map<std::string, size_t>;

using WordFrequenciesIt = std::unordered_map<std::string, size_t>::const_iterator;
struct WordFrequenciesItCmp {
    bool operator()(const WordFrequenciesIt& left, const WordFrequenciesIt& right) const
    {
        if (left->second == right->second)
            return left->first < right->first;
        return left->second > right->second;
    }
};

using SortedWordFrequencies =  std::set<WordFrequenciesIt, WordFrequenciesItCmp>;

bool fileExists(const std::string& file_path)
{
    std::ifstream file(file_path);
    return file.good();
}

WordFrequencies readWordFrequenciesFromInputFile(const std::string& file_path)
{
    std::unordered_map<std::string, size_t> word_frequencies;

    std::string word;
    WordFileReader word_reader(file_path);
    while (word = word_reader.readNext(), !word.empty())
    {
        word_frequencies[word] += 1;
    }

    return word_frequencies;
}

SortedWordFrequencies sortWordFrequencies(const WordFrequencies& word_frequencies)
{
    SortedWordFrequencies sorted_word_frequencies;
    for (auto it = word_frequencies.begin(); it != word_frequencies.end(); ++it)
    {
        sorted_word_frequencies.insert(it);
    }
    return sorted_word_frequencies;
}

bool printToFile(const std::string& file_path, const SortedWordFrequencies& sorted_word_frequencies)
{
    std::ofstream output_file(file_path, std::ofstream::trunc);
    if (!output_file.good())
    {
        return false;
    }

    for (auto& it : sorted_word_frequencies)
    {
        output_file << it->first << " - " << it->second << std::endl;
    }

    return true;
}


int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: ./word_frequency_calculator <path to file with words> <path to output file>" << std::endl;
        return 1;
    }

    if (!fileExists(argv[1]))
    {
        std::cout << "Failed to open " <<  argv[1] << std::endl;
        return 2;
    }

    auto word_frequencies = readWordFrequenciesFromInputFile(argv[1]);
    auto sorted_word_frequencies = sortWordFrequencies(word_frequencies);

    if (!printToFile(argv[2], sorted_word_frequencies))
    {
        std::cout << "Failed to print to " << argv[2] << std::endl;
        return 3;
    }

    return 0;
}