#pragma once

#include <fstream>

class WordFileReader
{
public:
    WordFileReader(const std::string& file_path);
    ~WordFileReader();
    std::string readNext();
private:
    std::ifstream input_file;
};